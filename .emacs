;; Enable MELPA
(require 'package)
(add-to-list 'load-path "~/.emacs.d/elpa")
(setq package-archives '(("gnu"
. "http://elpa.gnu.org/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")))
(package-initialize)

;; Path init
(setenv "PATH"
	(concat "/usr/local/bin" ":" "Library/TeX/texbin" ":"
		(getenv "PATH")))

;; Font
(set-face-attribute 'default t :family "Pragmata")
(set-frame-font "Pragmata TT" nil t)

;; Key bindings
(setq mac-command-modifier 'meta)
(require 'expand-region)
(global-set-key (kbd "C-=") 'er/expand-region)

;; Set up pretty symbols
(require 'pretty-mode)
(global-pretty-mode t)

;; Highlight delims
(setq show-paren-delay 0)
(show-paren-mode 1)

;; GUI shit
(if (display-graphic-p)
    (progn (tool-bar-mode 0)
	   (scroll-bar-mode 0)
	   (fringe-mode 0)))

;; programming general junk
(add-hook 'prog-mode-hook
	  (lambda ()
	    (progn
	      (rainbow-delimiters-mode t))))

;; #justlispthings (TODO: compartmentalize this section when I'm less lazy)
(add-hook 'lisp-mode-hook
	  (lambda ()
	    (progn
	      (paredit-mode t)
	      (slime-mode))))

;; #justhaskellthings (TODO: compartmentalize this section when I'm less lazy)
(require 'haskell-interactive-mode)
(require 'haskell-process)
(add-hook 'haskell-mode-hook 
	  ((lambda () 
	     (progn 
	       (interactive-haskell-mode)
	       (turn-on-haskell-indent)))))
(setq haskell-program-name "/usr/local/bin/ghci")
(define-key haskell-mode-map (kbd "C-c C-l") 'haskell-process-load-or-reload)
(define-key haskell-mode-map (kbd "C-`") 'haskell-interactive-bring)
(define-key haskell-mode-map (kbd "C-c C-t") 'haskell-process-do-type)
(define-key haskell-mode-map (kbd "C-c C-i") 'haskell-process-do-info)
(define-key haskell-mode-map (kbd "C-c C-c") 'haskell-process-cabal-build)
(define-key haskell-mode-map (kbd "C-c C-k") 'haskell-interactive-mode-clear)
(define-key haskell-mode-map (kbd "C-c c") 'haskell-process-cabal)
(define-key haskell-mode-map (kbd "SPC") 'haskell-mode-contextual-space)

;; elshit
(add-hook 'emacs-lisp-mode-hook
	  (lambda ()
	    (paredit-mode t)))

;; YAAAAAAAAAAAAAAAAAAS!
(yas-global-mode t)

;; AC
(global-auto-complete-mode t)

;; Shut the fuck up
(setq ring-bell-function 'ignore)

;; Don't backup
(setq backup-inhibited t)

;; Don't auto save
(setq auto-save-default nil)

;; PDFs
(setq doc-view-ghostscript-program "/usr/local/bin/gs")

;; ASM programming
(defun my-asm-mode-hook ()
  ;; you can use `comment-dwim' (M-;) for this kind of behaviour anyway
  (local-unset-key (vector asm-comment-char))
  ;; asm-mode sets it locally to nil, to "stay closer to the old TAB behaviour".
  (setq tab-always-indent (default-value 'tab-always-indent)))

(add-hook 'asm-mode-hook #'my-asm-mode-hook)

;; Eshell
(require 'em-smart)
(setq eshell-where-to-jump 'begin)
(setq eshell-review-quick-commands nil)
(setq eshell-smart-space-goes-to-end t)

;; tramp for sudo access
(require 'tramp)
(setq tramp-default-method "ssh")
(set-default 'tramp-default-proxies-alist (quote ((".*" "\\`root\\'" "/ssh:%h:"))))
(defun sudo-edit-current-file ()
  (interactive)
  (let ((position (point)))
    (find-alternate-file
     (if (file-remote-p (buffer-file-name))
         (let ((vec (tramp-dissect-file-name (buffer-file-name))))
           (tramp-make-tramp-file-name
            "sudo"
            (tramp-file-name-user vec)
            (tramp-file-name-host vec)
            (tramp-file-name-localname vec)))
       (concat "/sudo:root@localhost:" (buffer-file-name))))
    (goto-char position)))

;; ido-mode
(ido-mode t)
(setq ido-enable-prefix nil
      ido-enable-flex-matching t
      ido-create-new-buffer 'always
      ido-use-filename-at-point 'guess
      ido-max-prospects 10
      ido-default-file-method 'selected-window)

;; auto-completion in minibuffer
(icomplete-mode +1)
(set-default 'imenu-auto-rescan t)

; ;; smex
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)

;; mail
(setq gnus-select-method '(nnml ""))
(setq mail-sources '((pop :server "mail.riseup.net"
			  :user "resplendentrei@riseup.net"
			  :password "%btgEHgWYLJ*rHQD^ZFxXH6AwNBXAo"
			  :port 995
			  :stream tls)))
(setq send-mail-function 'smtpmail-send-it
      message-send-mail-function 'smtpmail-send-it
      smtpmail-default-smtp-server "mail.riseup.net"
      smtpmail-smtp-server "mail.riseup.net"
      smtpmail-local-domain nil
      smtpmail-stream-type 'tls
      smtpmail-smtp-service 465)
(setq user-mail-address "resplendentrei@riseup.net"
      user-full-name "綾波レイ")

;; SLIME
(require 'slime)
(setq slime-contribs '(slime-fancy))
(add-hook 'slime-repl-mode-hook 
	  (lambda () (progn
		  (rainbow-delimiters-mode t)
		  (pretty-mode t)
		  (paredit-mode t))))
(setq inferior-lisp-program "/usr/local/bin/sbcl")
(slime-setup '(slime-fancy))

;; Jedi
(add-hook 'python-mode-hook 'jedi:setup)
(setq jedi:complete-on-dot t)

;; Fireplace thing -- remove after xmas
(load "~/.emacs.d/fireplace/fireplace.elc")
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("561ba4316ba42fe75bc07a907647caa55fc883749ee4f8f280a29516525fc9e8" default))))

;; Theme
(load-theme 'cyberpunk)

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
